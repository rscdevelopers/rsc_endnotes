<?php

/**
 * @file
 * RSC Endnotes module.
 */

/**
 * Implements hook_filter_info().
 */
function rsc_endnotes_filter_info() {
  $filters['filter_rsc_endnotes'] = [
    'title' => t('Parse <code>!tag</code> tags', [
      '!tag' => htmlspecialchars('<endnote>'),
    ]),
    'description' => t('Replaces each <code>!tag</code> tag with a link to the endnote, and places the endnote content at the bottom of the HTML.', [
      '!tag' => htmlspecialchars('<endnote>'),
    ]),
    'process callback' => '_rsc_endnotes_filter_process',
  ];
  return $filters;
}

/**
 * Implmenets hook_permission().
 */
function rsc_endnotes_permission() {
  return [
    'administer RSC endnotes' => [
      'title' => t('Administer RSC endnotes'),
    ],
  ];
}

/**
 * Process callback for the filter_rsc_endnotes filter.
 *
 * @param string $text
 *   The HTML to process.
 * @param \stdClass $format
 *   The name of the text format in use.
 *
 * @return string
 *   The processed HTML.
 */
function _rsc_endnotes_filter_process(string $text, stdClass $format): string {
  if (empty($text)) {
    return $text;
  }

  module_load_include('php', 'rsc_common', 'phpQuery/phpQuery');
  $doc = phpQuery::newDocument($text);
  $n = 0;
  $endnotes = [];
  $endnote_tags = $doc['endnote'];
  if (!count($endnote_tags)) {
    // No endnotes, nothing to do.
    return $text;
  }

  foreach ($endnote_tags as $dom_node) {
    $n++;
    $endnote = pq($dom_node);
    $content = $endnote->html();

    // Preview a short version of the content.
    $preview = $endnote->text();
    if (strlen($preview) > 100) {
      $preview = substr($preview, 0, 99) . '…';
    }
    $preview = str_replace('"', '&quot;', $preview);
    $preview = str_replace("'", '&apos;', $preview);

    // FIXME: This might cause HTML ID conflicts when there is more than one
    // field that uses endnotes on a given page. We could solve that by adding
    // the field's delta and/or the node id to the ID, but we can't access that
    // here. Appending a random value is a very bad idea, since that causes
    // anchor URLs to change when saving the node.
    $ref_id = "endnote-ref-$n";
    $content_id = "endnote-content-$n";

    // Replace the endnote with a link to an anchor.
    $endnote->replaceWith("<sup class='rsc-endnotes reference' id='$ref_id'><a href='#$content_id' title='$preview'>$n</a></sup>");

    // Build content to add to footer later.
    $endnotes[$n] = <<<DOC
<li id="$content_id"><a href="#$ref_id" class="backlink" title="Jump back to article">^</a> $content</li>
DOC;
  }

  // Find or add article footer.
  $footer = $doc['footer'];
  if (count($footer)) {
    $footer = $footer->eq(0);
  }
  else {
    $doc->append("<footer></footer>");
    $footer = $doc['footer']->eq(0);
  }

  // Add endnotes to footer.
  $endnotes_html = "<h2>Endnotes</h2><ol class='rsc-endnotes' type='1'>" . implode("\n", $endnotes) . "</ol>";
  $footer->append($endnotes_html);

  return (string) $doc;
}
