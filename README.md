# rsc_endnotes

Defines a text filter that parses HTML,
replacing each <endnote> tag with a link to the endnote,
and places the endnote content at the bottom of the html.
